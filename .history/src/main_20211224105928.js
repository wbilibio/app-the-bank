import Vue from 'vue'
import App from './App.vue'
import router from '@/router'
import store from './store/store'

/** Main Css */
import './assets/scss/main.scss'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  store,
  router,
  render: h => h(App),  
}).$mount('#app')
