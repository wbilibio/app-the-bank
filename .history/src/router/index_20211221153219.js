import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/views/Home'
import Transfer from '@/views/Transfer'

Vue.use(Router)

const routes = [
    {
        name: 'home',
        path: '/',
        component: Home
    },
    {
        name: 'tranfer',
        path: '/',
        component: Transfer
    }
];

const router = new Router({ routes })

export default router