import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/views/Login'
import Home from '@/views/Home'
import Transfer from '@/views/Transfer'

Vue.use(Router)

const routes = [
    {
        name: 'login',
        path: '/',
        component: Login
    },
    {
        name: 'home',
        path: '/home',
        component: Home
    },
    {
        name: 'tranfer',
        path: '/transfer',
        component: Transfer
    },
    {
        name: 'deposit',
        path: '/deposit',
        component: Deposit
    },
    {
        name: 'account',
        path: '/account',
        component: Account
    },
    {
        name: 'withdraw',
        path: '/withdraw',
        component: Withdraw
    }
];

const router = new Router({ routes })

export default router