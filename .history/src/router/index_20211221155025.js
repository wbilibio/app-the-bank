import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/views/Login'
import Transfer from '@/views/Transfer'

Vue.use(Router)

const routes = [
    {
        name: 'login',
        path: '/',
        component: Login
    },
    {
        name: 'tranfer',
        path: '/transfer',
        component: Transfer
    }
];

const router = new Router({ routes })

export default router