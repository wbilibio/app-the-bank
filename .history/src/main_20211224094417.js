import Vue from 'vue'
import App from './App.vue'
import Router from '@/router'
import Store from './store/store'

/** Main Css */
import './assets/scss/main.scss'

Vue.config.productionTip = false


new Vue({
  Store,
  Router,
  render: h => h(App),
  
}).$mount('#app')
