import Vue from 'vue'
import App from './App.vue'
import router from '@/router'
/** Main Css */
import './assets/scss/main.scss'

Vue.config.productionTip = false


new Vue({
  render: h => h(App),
  router
}).$mount('#app')
