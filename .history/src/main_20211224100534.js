import Vue from 'vue'
import App from './App.vue'
import Router from '@/router'
import Store from './store/store'

/** Main Css */
import './assets/scss/main.scss'

new Vue({
  Store,
  Router,
  render: h => h(App),  
}).$mount('#app')
