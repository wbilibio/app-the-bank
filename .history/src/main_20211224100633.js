import Vue from 'vue'
import App from './App.vue'
import router from '@/router'
import Store from './store/store'

/** Main Css */
import './assets/scss/main.scss'

new Vue({
  Store,
  router,
  render: h => h(App),  
}).$mount('#app')
