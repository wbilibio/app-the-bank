import Vue from 'vue'
import App from './App.vue'
import router from '@/router'
import Main from './assets/scss/_main'

Vue.config.productionTip = false


new Vue({
  render: h => h(App),
  router
}).$mount('#app')
