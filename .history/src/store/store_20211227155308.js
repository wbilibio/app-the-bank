import Vue from "vue";
import Vuex from "vuex";
import base from '../plugins/axios';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        linksMenu: ['Account', 'Transfer', 'Deposit', 'Withdraw'],
        fixed_historic: [
            {image:'../assets/images/icon-circle-eua.png',type_text:'TRANSFER USD',type:'negative', name:'Richard Bronson', amount:'-$100', date: 'TODAY'},
            {image:'../assets/images/icon-circle-eua.png',type_text:'DEPOSIT USD',type:'positive', name:'', amount:'$200', date: 'YESTERDAY'},
            {image:'../assets/images/icon-circle-eua.png',type_text:'RECEIVED USD',type:'positive', name:'Richard Bronson', amount:'$100', date: 'YESTERDAY'},
            {image:'../assets/images/icon-circle-eua.png',type_text:'TRANSFER USD',type:'positive', name:'Richard Bronson', amount:'$100', date: 'YESTERDAY'}
        ],
        historic: [],
        showMenu: false,
        filter: {
            start_date: null,
            end_date: null,
            search_by: null,
            statement_type: null,
        },
        account: {
            agency: null,
            account: null,
            amount: null
        }
    },
    getters: {
        getLinksMenu: (state) => state.linksMenu,
        getShowMenu: (state) => state.showMenu,
        getFixedHistoric: (state) => state.fixed_historic,
        getFilter: (state) => state.filter,
    },
    mutations: {
        setLinksMenu: (state, payload) => {
            state.linksMenu = payload;
        },
        setShowMenu: (state, payload) => {
            state.showMenu = payload;
        },
        setHistoric: (state, payload) => {
            state.historic = payload;
        },
        setFilter: (state, payload) => {
            state.filter = payload;
        }
    },
    actions: {
        /** Get Historic for date, search and type */
        async SearchHistoric({ commit, state }) {
            const data = await base
                .get("/statements/historic", {
                    params: {
                        start_date: state.filter.start_date,
                        end_date: state.filter.start_date,
                        search_by: state.filter.search_by,
                        statement_type: state.filter.statement_type,
                    }
                })
                .then(({ data }) => {
                    if (data.status && data.status === 'error') return [];
                    return data
                });
            commit("setHistoric", data);
        },
        /** Deposit amount */
        async DepositAmount({ commit, state }) {
            const data = await base
                .get("/statements/deposit", {
                    "agency": state.account.agency,
                    "account": state.account.account,
                    "amount": state.account.amount
                  })
                .then(({ data }) => {
                    if (data.status && data.status === 'error') return [];
                    return data
                });
            commit("setDeposit", data);
        }
    }    
});