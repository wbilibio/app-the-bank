import Vue from "vue";
import Vuex from "vuex";
import base from '../plugins/axios';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        linksMenu: ['Account', 'Transfer', 'Deposit', 'Withdraw'],
        colleges: [
            { name: 'Estácio', slug: "estacio", id: "47" }
        ],
        academicLevels: [
            { name: 'Graduação', slug: "graduacao", id: "151966" },
            { name: 'Pós graduação', slug: "pos-graduacao", id: "151957" },
            { name: 'Tecnólogo', slug: "tecnologo", id: "151958" },
            { name: 'Cursos livres', slug: "cursos-livres", id: "151958" },
        ],
        filter: {
            course: null,
            state: null,
            city: null,
            modality: null,
            shift: null,
            college: null,
            academicLevel: "graduacao",
            priceRange: {
                max: null,
                min: null,
            },
        },
    },
    getters: {
        getAcademicLevels: (state) => state.academicLevels,
        getCourses: (state) => state.courses,
        getCities: (state) => state.cities,
        getStates: (state) => state.states,
        getColleges: (state) => state.colleges,
        getCampus: (state) => state.campus,
        getModalities: (state) => state.modalities,
        getShifts: (state) => state.shifts,
        getScholarships: (state) => state.scholarships,
        getFilter: (state) => state.filter,
        getFilterAcademicLevel: (state) => state.filter.academicLevel,
    },
    mutations: {
        setFilterAcademicLevel: (state, payload) => {
            state.filter.academicLevel = payload;
        },
        setFilterCity: (state, payload) => {
            state.filter.city = payload;
        },
        setFilterCourse: (state, payload) => {
            state.filter.course = payload;
        },
        setFilterState: (state, payload) => {
            state.filter.state = payload;
        },
        setFilterModality: (state, payload) => {
            state.filter.modality = payload;
        },
        setFilterShift: (state, payload) => {
            state.filter.shift = payload;
        },
        setFilterCollege: (state, payload) => {
            state.filter.college = payload;
        },
        setFilterCampus: (state, payload) => {
            state.filter.campus = payload;
        },
        setFilterPriceRange: (state, payload) => {
            state.filter.priceRange = payload;
        },
        setCourses: (state, payload) => {
            state.courses = payload;
        },
        setAcademicLevels: (state, payload) => {
            state.academicLevels = payload;
        },
        setCities: (state, payload) => {
            state.cities = payload;
        },
        setColleges: (state, payload) => {
            state.colleges = payload;
        },
        setModalities: (state, payload) => {
            state.modalities = payload;
        },
        setShifts: (state, payload) => {
            state.shifts = payload;
        },
        setCampusList: (state, payload) => {
            state.campus = payload;
        },
        setScholarships: (state, payload) => {
            state.scholarships = payload;
        }
    },
    actions: {
        async SearchCourses({ commit, state }) {
            const academicLevel = state.academicLevels.find(a => a.slug === state.filter.academicLevel);
            const academicLevelId = academicLevel.id || '';
            const data = await base
                .post("/get-courses", {
                    nivel_academico: academicLevelId
                })
                .then(({ data }) => {
                    if (data.status && data.status === 'error') return [];
                    return data
                });
            commit("setCourses", data);
        },
        async SearchCities({ commit }) {
            const data = await base
                .post("/list-attributes", { type: "pa_municipio" })
                .then(({ data }) => {
                    if (data.status && data.status === 'error') return [];
                    return data
                });
            commit("setCities", data);
        },
        async SearchShifts({ commit }) {
            const data = await base
                .post("/list-attributes", { type: "pa_turno" })
                .then(({ data }) => {
                    if (data.status && data.status === 'error') return [];
                    return data
                });
            commit("setShifts", data);
        },
        async SearchModalities({ commit }) {
            const data = await base
                .post("/list-attributes", { type: "pa_modalidade" })
                .then(({ data }) => {
                    if (data.status && data.status === 'error') return [];
                    return data
                });
            commit("setModalities", data);
        },
        async SearchCampus({ commit, state }) {
            const data = await base
                .post("/get-campus", { nivel_academico: state.filter.academicLevel, categoria: state.filter.college, municipio: state.filter.city, titulo: state.filter.course })
                .then(({ data }) => {
                    if (data.status && data.status === 'error') return [{ name: "Não há campus para as opções selecionadas", slut: "" }];
                    return data
                });
            commit("setCampusList", data);
        },
        async SearchScholarships({ commit, state }) {
            const data = await base
                .post("/get-scholarships", {
                    nivel_academico: state.filter.academicLevel,
                    categoria: state.filter.college,
                    municipio: state.filter.city,
                    titulo: state.filter.course,
                    modalidade: state.filter.modality,
                    turno: state.filter.shift,
                    campus: state.filter.campus,
                })
                .then(({ data }) => {
                    if (data.status && data.status === 'error') return [];
                    return data
                });
            commit("setScholarships", data);
        },
    },
});