import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        linksMenu: ['Account', 'Transfer', 'Deposit', 'Withdraw'],
        histories: [
            {image:'@/assets/images/icon-circle-eua.png',type:'TRANSFER USD', name:'Richard Bronson', value:'-$100', date: 'TODAY'},
            {image:'@/assets/images/icon-circle-eua.png',type:'DEPOSIT USD', name:'', value:'$200', date: 'YESTERDAY'},
            {image:'@/assets/images/icon-circle-eua.png',type:'RECEIVED USD', name:'Richard Bronson', value:'$100', date: 'YESTERDAY'},
            {image:'@/assets/images/icon-circle-eua.png',type:'TRANSFER USD', name:'Richard Bronson', value:'$100', date: 'YESTERDAY'}
        ],
        showMenu: false
    },
    getters: {
        getLinksMenu: (state) => state.linksMenu,
        getShowMenu: (state) => state.showMenu,
    },
    mutations: {
        setLinksMenu: (state, payload) => {
            state.linksMenu = payload;
        },
        setShowMenu: (state, payload) => {
            state.showMenu = payload;
        }
    }
});