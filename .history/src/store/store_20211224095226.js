import Vue from "vue";
import Vuex from "vuex";
import base from '../plugins/axios';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        linksMenu: ['Account', 'Transfer', 'Deposit', 'Withdraw']
    },
    getters: {
        getLinksMenu: (state) => state.linksMenu,
    },
    mutations: {
        setLinksMenu: (state, payload) => {
            state.linksMenu = payload;
        }
    },
    actions: {
        async SearchCourses({ commit, state }) {
            const academicLevel = state.academicLevels.find(a => a.slug === state.filter.academicLevel);
            const academicLevelId = academicLevel.id || '';
            const data = await base
                .post("/get-courses", {
                    nivel_academico: academicLevelId
                })
                .then(({ data }) => {
                    if (data.status && data.status === 'error') return [];
                    return data
                });
            commit("setCourses", data);
        },
        async SearchCities({ commit }) {
            const data = await base
                .post("/list-attributes", { type: "pa_municipio" })
                .then(({ data }) => {
                    if (data.status && data.status === 'error') return [];
                    return data
                });
            commit("setCities", data);
        },
        async SearchShifts({ commit }) {
            const data = await base
                .post("/list-attributes", { type: "pa_turno" })
                .then(({ data }) => {
                    if (data.status && data.status === 'error') return [];
                    return data
                });
            commit("setShifts", data);
        },
        async SearchModalities({ commit }) {
            const data = await base
                .post("/list-attributes", { type: "pa_modalidade" })
                .then(({ data }) => {
                    if (data.status && data.status === 'error') return [];
                    return data
                });
            commit("setModalities", data);
        },
        async SearchCampus({ commit, state }) {
            const data = await base
                .post("/get-campus", { nivel_academico: state.filter.academicLevel, categoria: state.filter.college, municipio: state.filter.city, titulo: state.filter.course })
                .then(({ data }) => {
                    if (data.status && data.status === 'error') return [{ name: "Não há campus para as opções selecionadas", slut: "" }];
                    return data
                });
            commit("setCampusList", data);
        },
        async SearchScholarships({ commit, state }) {
            const data = await base
                .post("/get-scholarships", {
                    nivel_academico: state.filter.academicLevel,
                    categoria: state.filter.college,
                    municipio: state.filter.city,
                    titulo: state.filter.course,
                    modalidade: state.filter.modality,
                    turno: state.filter.shift,
                    campus: state.filter.campus,
                })
                .then(({ data }) => {
                    if (data.status && data.status === 'error') return [];
                    return data
                });
            commit("setScholarships", data);
        },
    },
});