import Vue from "vue";
import Vuex from "vuex";
import base from '../plugins/axios';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        linksMenu: ['Account', 'Transfer', 'Deposit', 'Withdraw'],
        fixed_historic: [
            {image:'../assets/images/icon-circle-eua.png',type_text:'TRANSFER USD',type:'negative', name:'Richard Bronson', amount:'-$100', date: 'TODAY'},
            {image:'../assets/images/icon-circle-eua.png',type_text:'DEPOSIT USD',type:'positive', name:'', amount:'$200', date: 'YESTERDAY'},
            {image:'../assets/images/icon-circle-eua.png',type_text:'RECEIVED USD',type:'positive', name:'Richard Bronson', amount:'$100', date: 'YESTERDAY'},
            {image:'../assets/images/icon-circle-eua.png',type_text:'TRANSFER USD',type:'positive', name:'Richard Bronson', amount:'$100', date: 'YESTERDAY'}
        ],
        historic: [],
        showMenu: false,
        filter: {
            start_date: null,
            end_date: null,
            search_by: null,
            statement_type: null,
        },
        account: {
            full_name: null,
            balance: null,
            agency: null,
            number_account: null
        },
        data_person: {
            full_name: null,
            birth_date: null,
            document: null,
            email: null,
            password: null,
            cellphone: null,
            address: {
                country: null,
                state: null,
                city: null,
                neighborhood: null,
                street: null,
                number: null,
                complement: null
            }
        }
    },
    getters: {
        getLinksMenu: (state) => state.linksMenu,
        getShowMenu: (state) => state.showMenu,
        getFixedHistoric: (state) => state.fixed_historic,
        getFilter: (state) => state.filter,
    },
    mutations: {
        setLinksMenu: (state, payload) => {
            state.linksMenu = payload;
        },
        setShowMenu: (state, payload) => {
            state.showMenu = payload;
        },
        setHistoric: (state, payload) => {
            state.historic = payload;
        },
        setFilter: (state, payload) => {
            state.filter = payload;
        },
        setAccount: (state, payload) => {
            state.account = payload;
        },
    },
    actions: {
        /** Get Historic for date, search and type */
        async SearchHistoric({ commit, state }) {
            const data = await base
                .get("/statements/historic", {
                    params: {
                        start_date: state.filter.start_date,
                        end_date: state.filter.start_date,
                        search_by: state.filter.search_by,
                        statement_type: state.filter.statement_type,
                    }
                })
                .then(({ data }) => {
                    if (data.status && data.status === 'error') return [];
                    return data
                });
            commit("setHistoric", data);
        },
        /** Get Account Data */
        async Account({ commit }) {
            const data = await base
                .get("/account")
                .then(({ data }) => {
                    if (data.status && data.status === 'error') return [];
                    return data
                });
            commit("setAccount", data);
        },
        /** Get Data Person */
        async Person({ commit }) {
            const data = await base
                .get("/me")
                .then(({ data }) => {
                    if (data.status && data.status === 'error') return [];
                    return data
                });
            commit("setAccount", data);
        },
        /** Signup */
        async Signup({ state }) {
            const data = await base
                .post("/me", {
                    "email": state.data_person.email,
                    "password": state.data_person.password,
                    "user_information": {
                        "full_name": state.data_person.full_name,
                        "document": state.data_person.document,
                        "birth_date": state.data_person.birth_date,
                        "cellphone": state.data_person.cellphone
                    },
                    "user_address": {
                        "country": state.data_person.address.country,
                        "state": state.data_person.address.state,
                        "city": state.data_person.address.city,
                        "neightborhood": state.data_person.address.neighborhood,
                        "street": state.data_person.address.street,
                        "number": state.data_person.address.number,
                        "zip_code": state.data_person.address.zip_code,
                        "complement": state.data_person.address.complement,
                    }
                })
                .then(({ data }) => {
                    if (data.status && data.status === 'error') return [];
                    return data
                });
        },
        async Signup({ commit, state }) {
            const data = await base
                .post("/me", {
                    "email": state.data_person.email,
                    "password": state.data_person.password
                })
                .then(({ data }) => {
                    if (data.status && data.status === 'error') return [];
                    return data
                });
            commit("setAccount", data);
        }
    }    
});