import Vue from "vue";
import Vuex from "vuex";
import base from '../plugins/axios';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        linksMenu: ['Account', 'Transfer', 'Deposit', 'Withdraw'],
        historic: [
            {image:'../assets/images/icon-circle-eua.png',type_text:'TRANSFER USD',type:'negative', name:'Richard Bronson', amount:'-$100', date: 'TODAY'},
            {image:'../assets/images/icon-circle-eua.png',type_text:'DEPOSIT USD',type:'positive', name:'', amount:'$200', date: 'YESTERDAY'},
            {image:'../assets/images/icon-circle-eua.png',type_text:'RECEIVED USD',type:'positive', name:'Richard Bronson', amount:'$100', date: 'YESTERDAY'},
            {image:'../assets/images/icon-circle-eua.png',type_text:'TRANSFER USD',type:'positive', name:'Richard Bronson', amount:'$100', date: 'YESTERDAY'}
        ],
        showMenu: false
    },
    getters: {
        getLinksMenu: (state) => state.linksMenu,
        getShowMenu: (state) => state.showMenu,
        getHistoric: (state) => state.historic,
    },
    mutations: {
        setLinksMenu: (state, payload) => {
            state.linksMenu = payload;
        },
        setShowMenu: (state, payload) => {
            state.showMenu = payload;
        },
        setHistoric: (state, payload) => {
            state.historic = payload;
        }
    },
    actions: {
        /**  */
        async SearchHistoric({ commit, state }) {
            const data = await base
                .get("/statements/historic", {
                    start_date: "2021-12-01",
                    categoria: state.filter.college,
                    municipio: state.filter.city,
                    titulo: state.filter.course,
                    modalidade: state.filter.modality,
                    turno: state.filter.shift,
                    campus: (state.campus) === undefined ? null : state.campus,
                })
                .then(({ data }) => {
                    if (data.status && data.status === 'error') return [];
                    return data
                });
            commit("setHistoric", data);
        }
    }    
});