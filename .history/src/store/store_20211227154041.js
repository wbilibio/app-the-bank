import Vue from "vue";
import Vuex from "vuex";
import base from '../plugins/axios';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        linksMenu: ['Account', 'Transfer', 'Deposit', 'Withdraw'],
        fixed_historic: [
            {image:'../assets/images/icon-circle-eua.png',type_text:'TRANSFER USD',type:'negative', name:'Richard Bronson', amount:'-$100', date: 'TODAY'},
            {image:'../assets/images/icon-circle-eua.png',type_text:'DEPOSIT USD',type:'positive', name:'', amount:'$200', date: 'YESTERDAY'},
            {image:'../assets/images/icon-circle-eua.png',type_text:'RECEIVED USD',type:'positive', name:'Richard Bronson', amount:'$100', date: 'YESTERDAY'},
            {image:'../assets/images/icon-circle-eua.png',type_text:'TRANSFER USD',type:'positive', name:'Richard Bronson', amount:'$100', date: 'YESTERDAY'}
        ],
        historic: [],
        showMenu: false
    },
    getters: {
        getLinksMenu: (state) => state.linksMenu,
        getShowMenu: (state) => state.showMenu,
        getFixedHistoric: (state) => state.fixed_historic,
    },
    mutations: {
        setLinksMenu: (state, payload) => {
            state.linksMenu = payload;
        },
        setShowMenu: (state, payload) => {
            state.showMenu = payload;
        },
        setHistoric: (state, payload) => {
            state.historic = payload;
        }
    },
    actions: {
        /**  */

        }
    }    
});