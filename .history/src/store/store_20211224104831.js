import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        linksMenu: [
            { name: 'Graduação', slug: "graduacao", id: "151966" }
        ]
    },
    getters: {
        getLinksMenu: (state) => state.linksMenu,
    },
    mutations: {
        setLinksMenu: (state, payload) => {
            state.linksMenu = payload;
        }
    }
});