import Vue from "vue";
import Vuex from "vuex";
import base from '../plugins/axios';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        linksMenu: ['Account', 'Transfer', 'Deposit', 'Withdraw']
    },
    getters: {
        getLinksMenu: (state) => state.linksMenu,
    },
    mutations: {
        setLinksMenu: (state, payload) => {
            state.linksMenu = payload;
        }
    },
    actions: {
        
    },
});