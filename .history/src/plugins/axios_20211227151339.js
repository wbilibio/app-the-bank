// eslint-disable-next-line no-unused-vars
import axios from 'axios'

const base = axios.create({
    baseURL: 'https://api.bank.com/',
})

export default base