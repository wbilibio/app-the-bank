// eslint-disable-next-line no-unused-vars
import axios from 'axios'

const base = axios.create({
    baseURL: 'https://store.vaidebolsa.com.br/wp-json/api/',
})

export default base