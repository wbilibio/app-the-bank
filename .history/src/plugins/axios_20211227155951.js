// eslint-disable-next-line no-unused-vars
import axios from 'axios'

const token = '';

const base = axios.create({
    baseURL: 'https://api.bank.com/',
    {
        headers: {
            'Authorization': 'Barer' + token
        },
    }
})

export default base